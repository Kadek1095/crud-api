import 'package:flutter/material.dart';
import 'app.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark, primaryColor: Colors.cyan[800]),
      home: App(),
    ));
